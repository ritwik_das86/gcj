package gcj;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Author: Ritwik
 * Date: 11/18/2015
 */
public abstract class Problems {
    private List<String> inputLines;
    private SortedMap<Integer, String> outputLines;
    ExecutorService executorService;

    public Problems(String input, String output) {
        inputLines = new ArrayList<>();
        outputLines = Collections.synchronizedSortedMap(new TreeMap<>());
        executorService = Executors.newWorkStealingPool();
        readInput(input);
        runTestCases();
        writeResult(output);
    }

    private void writeResult(String file) {
        try {
            // Assume default encoding.
            FileWriter fileWriter = new FileWriter(file);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            Iterator<String> it = outputLines.values().iterator();
            while (it.hasNext()) {
                bufferedWriter.write(it.next());
                bufferedWriter.newLine();
            }

            // Always close files.
            bufferedWriter.close();
        } catch (IOException ex) {
            System.out.println("Error writing to file '" + file + "'");
            ex.printStackTrace();
        }
    }

    private void readInput(String file) {
        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(file);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                inputLines.add(line);
            }

            // Always close files.
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file '" + file + "'");
        } catch (IOException ex) {
            System.out.println("Error reading file '" + file + "'");
            // Or we could just do this:
            ex.printStackTrace();
        }
    }

    private void runTestCases() {
        int cases = Integer.valueOf(inputLines.get(0));
        int line = 1;
        for(int tc = 1; tc <= cases; tc++) {
            int nLines = getTCLines(inputLines.get(line));
            executorService.execute(new TestCaseProcessor(tc, line, nLines));
            line += nLines;
        }
        executorService.shutdown();
        try {
            while (!executorService.isTerminated()) {
                executorService.awaitTermination(20, TimeUnit.SECONDS);
            }
        } catch (InterruptedException e) {
            System.out.println("runTestCases thread was interrupted.");
        }
    }

    private class TestCaseProcessor implements Runnable {
        private int tc;
        private int startLine;
        private int nLines;

        public TestCaseProcessor(int tc, int startLine, int nLines) {
            this.tc = tc;
            this.startLine = startLine;
            this.nLines = nLines;
        }

        @Override
        public void run() {
            System.out.println(tc + ": " + inputLines.subList(startLine, startLine + nLines));
            addTCResult(tc, processTestCase(inputLines.subList(startLine, startLine + nLines)));
        }
    }

    protected abstract String processTestCase(List<String> lines);

    protected abstract int getTCLines(String line);

    private void addTCResult(int tc, String result) {
        outputLines.put(tc, "Case #" + tc + ": " + result);
        System.out.println(outputLines.get(tc));
    }
}