package gcj.gcj2015.Round2;

import gcj.Problems;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Author: Ritwik
 * Date: 11/18/2015
 */
public class ProbB extends Problems {
    public ProbB(String input, String output) {
        super(input, output);
    }

    @Override
    protected String processTestCase(List<String> lines) {
        String[] params = lines.get(0).split(" ");
        int n = lines.size() - 1;
        double targetVol = Double.valueOf(params[1]);
        double targetTemp = Double.valueOf(params[2]);
        double maxTemp = 0.0;
        double minTemp = 100.0;
        NavigableMap<Double, Double> colder = new TreeMap<>();
        NavigableMap<Double, Double> hotter = new TreeMap<>();
        double selfRate = 0.0;
        for(int i = 1; i < lines.size(); i++) {
            String[] RC = lines.get(i).split(" ");
            double rate = Double.valueOf(RC[0]);
            double temp = Double.valueOf(RC[1]);
            if(temp > maxTemp) {
                maxTemp = temp;
            }
            if(temp < minTemp) {
                minTemp = temp;
            }

            if(temp < targetTemp) {
                if(colder.containsKey(temp)) {
                    colder.put(temp, colder.get(temp) + rate);
                } else {
                    colder.put(temp, rate);
                }
            } else if(temp > targetTemp){
                if(hotter.containsKey(temp)) {
                    hotter.put(temp, hotter.get(temp) + rate);
                } else {
                    hotter.put(temp, rate);
                }
            } else {
                selfRate += rate;
            }
        }

        // Check for impossible
        if(targetTemp > maxTemp || targetTemp < minTemp) {
            return "IMPOSSIBLE";
        }

        if(n == 1) {
            return String.valueOf(targetVol / selfRate);
        }
        double minTime = selfRate == 0.0 ? Double.MAX_VALUE : targetVol / selfRate;
        Map.Entry<Double, Double> hot = hotter.pollLastEntry();
        Map.Entry<Double, Double> cold = colder.pollLastEntry();
        if(hot != null && cold != null) {
            Double hotRate = 0.0, hotTemp = 0.0, coldRate = selfRate, coldTemp = targetTemp;
            while (true) {
                if (hot != null) {
                    hotTemp = ((hotTemp * hotRate) + (hot.getKey() * hot.getValue())) / (hotRate + hot.getValue());
                    hotRate += hot.getValue();
                }
                if (cold != null) {
                    coldTemp = ((coldTemp * coldRate) + (cold.getKey() * cold.getValue())) / (coldRate + cold.getValue());
                    coldRate += cold.getValue();
                }
                double timeC = targetVol / coldRate * (targetTemp - hotTemp) / (coldTemp - hotTemp);
                double timeH = (targetVol - coldRate * timeC) / hotRate;
                double max = Math.max(timeC, timeH);
                if (max < minTime) {
                    minTime = max;
                }
                if (timeC > timeH) {
                    cold = colder.pollLastEntry();
                    if (cold == null)
                        break;
                    hot = null;
                } else {
                    hot = hotter.pollLastEntry();
                    if (hot == null)
                        break;
                    cold = null;
                }
            }
        }
        return String.valueOf(minTime);
    }

    @Override
    protected int getTCLines(String line) {
        int rows = Integer.valueOf(line.split(" ")[0]);
        return rows + 1;
    }
}
