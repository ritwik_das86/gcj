package gcj.gcj2015.Round2;

import gcj.Problems;

import java.util.List;

/**
 * Author: Ritwik
 * Date: 11/18/2015
 */
public class ProbA extends Problems {

    public ProbA(String input, String output) {
        super(input, output);
    }

    @Override
    protected String processTestCase(List<String> lines) {
        String result = "IMPOSSIBLE";
        int res = -1;
        int rows = lines.size() - 1;
        int cols = lines.get(1).length();
        for(int i = 1; i <= rows; i++) {
            for(int j = 0; j < cols; j++) {
                int pathlen = 0;
                char cur = lines.get(i).charAt(j);
                if(cur == '.' || cur =='p')
                    continue;
                int ni = i;
                int nj = j;
                char dir = cur;
                boolean oob = false;
                while(cur != 'p') {
                    switch (cur) {
                        case 'v':
                            ni++;
                            break;
                        case '^':
                            ni--;
                            break;
                        case '<':
                            nj--;
                            break;
                        case '>':
                            nj++;
                            break;
                    }
                    if(ni < 1 || ni > rows || nj < 0 || nj >= cols) {
                        oob = true;
                        break;
                    }
                    pathlen++;
                    cur = lines.get(ni).charAt(nj);
                }
            }
        }
        return result;
    }

    @Override
    protected int getTCLines(String line) {
        int rows = Integer.valueOf(line.split(" ")[0]);
        return rows + 1;
    }
}
