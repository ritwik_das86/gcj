import gcj.gcj2015.Round2.ProbA;
import gcj.gcj2015.Round2.ProbB;

/**
 * Author: Ritwik
 * Date: 11/18/2015
 */
public class RunCode {
    public static void main(String[] args) {
        new ProbB("sample.txt", "B-large.out");
        System.out.println("Done");
    }
}
